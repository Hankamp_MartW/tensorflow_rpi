#import tensorflow as tf
#import numpy as np

def get_label(file_path):
  part = tf.strings.split(input=file_path, sep=os.path.sep)
  return part[-2]

def decode_audio(audio_binary):
  # Decode WAV-encoded audio files to `float32` tensors, normalized
  # to the [-1.0, 1.0] range. Return `float32` audio and a sample rate.
  audio, _ = tf.audio.decode_wav(contents=audio_binary)
  # Since all the data is single channel (mono), drop the `channels`
  # axis from the array.
  return tf.squeeze(audio, axis=-1) #tfl.squeeze

def get_spectrogram(waveform):
  # Zero-padding for an audio waveform with less than 16,000 samples.
  input_len = 16000
  waveform = waveform[:input_len]
  #print("Waveform is:", waveform)
  zero_padding = tf.zeros(
      [16000] - tf.shape(waveform),
      dtype=tf.float32)
  #print("Zero padding of waveform:", zero_padding)
  # Cast the waveform tensors' dtype to float32.
  waveform = tf.cast(waveform, dtype=tf.float32)
  #print("Waveform after float32 casting is:", waveform)
  # Concatenate the waveform with `zero_padding`, which ensures all audio
  # clips are of the same length.
  equal_length = tf.concat([waveform, zero_padding], 0)
  #print("Equal length concat is:", equal_length)
  # Convert the waveform to a spectrogram via a STFT.
  spectrogram = tf.signal.stft(
      equal_length, frame_length=255, frame_step=128)
  #print("Spectogram is:", spectrogram)
  # Obtain the magnitude of the STFT.
  spectrogram = tf.abs(spectrogram)
  #print("Spectogram absolute/magnitude is:", spectrogram)
  # Add a `channels` dimension, so that the spectrogram can be used
  # as image-like input data with convolution layers (which expect
  # shape (`batch_size`, `height`, `width`, `channels`).
  spectrogram = spectrogram[..., tf.newaxis]
  #print("Spectogram with added channel dimension is:", spectrogram)
  return spectrogram

def get_spectrogram_and_label(file_path):
  label = get_label(file_path)
  print("File path is: ", file_path)
  label_id = tf.argmax(label == commands)
  print("The label ID is: ", label_id)

  audio_binary = tf.io.read_file(file_path)
  print("Audio Binaray is: ", audio_binary)
  waveform = decode_audio(audio_binary)

  spectrogram = get_spectrogram(waveform)

  return spectrogram, label_id

if __name__ == '__main__':
  AUTOTUNE = tf.data.AUTOTUNE
  files_ds = tf.data.Dataset.from_tensor_slices(filenames)
  print(files_ds)

  test_ds = files_ds.map(
    map_func=(lambda file_path: get_spectrogram_and_label(file_path)), num_parallel_calls=AUTOTUNE)
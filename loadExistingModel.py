#import tensorflow as tf

if __name__ == '__main__':
    model_path = "C:\Hankamp\Saved_models\V0.04_WhiteNoise10dB_WithallUnknown_speech_command_dataset"   #Personal Desktop
    #model_path = "E:\Hankamp\RaspberryPi Zero\V0.04_WhiteNoise10dB_WithallUnknown_speech_command_dataset" #Personal Laptop
    #model_path = "/home/pi/tensorflow_models/...." #Raspbery Pi

    #model = keras.model.load_model(model_path)
    model = tf.keras.models.load_model(model_path)
    model.summary()

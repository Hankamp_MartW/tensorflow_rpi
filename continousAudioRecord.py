import pyaudio
import numpy as np
import pylab
import time
import sys
import matplotlib.pyplot as plt
import collections
import itertools

device = 3
RATE = 44100
CHUNK = int(RATE/20) # RATE / number of updates per second

frameLength = CHUNK*2
hopLength = CHUNK
audioBuffer = collections.deque()

def soundplot(stream):
    start = time.perf_counter()
    data = np.fromstring(stream.read(CHUNK),dtype=np.int16)
    audioBuffer.extend(data)
    #print(len(data))
    print("Time needed to read in ms: ", (time.perf_counter() - start)*1000)
    #print("time is: ", t1)

def preProcessingSimulation():
    print("Pre-processing:")
    #audioSlice = printAmountBuffer(audioBuffer, frameLength)
    #print("Audio slice is: ", audioSlice)
    #print("Average of these samples: ", np.average(audioSlice))

    time.sleep(0.12)

    print("Buffer size before delete", len(audioBuffer))
    start = time.perf_counter()
    delete_x_amount(audioBuffer, hopLength)
    print("Time needed to delete in ms: ", (time.perf_counter() - start)*1000)
    print("Buffer size after delete", len(audioBuffer))

    print("Done Pre-processing")

def printAmountBuffer(buffer, length):
    bufferPrint = []
    for character in range(length-1):
        bufferPrint.append(buffer[character])
    return bufferPrint

def delete_x_amount(d, x):
    for itteration in range(x):
        d.popleft()

def delete_nth(d, n):
    d.rotate(-n)
    d.popleft()
    d.rotate(n)
    
if __name__=="__main__":
    p=pyaudio.PyAudio()
    stream=p.open(format=pyaudio.paInt16,input_device_index = device, channels=1,rate=RATE,input=True,
                  frames_per_buffer=CHUNK)
    for i in range(sys.maxsize**10):
        soundplot(stream)
        print("Audio buffer after recording is: ", len(audioBuffer))
        if len(audioBuffer) >= frameLength:
            preProcessingSimulation()
        #print("itteration number: ", i)
    stream.stop_stream()
    stream.close()
    p.terminate()
    print("Ik sluit hem nu!")
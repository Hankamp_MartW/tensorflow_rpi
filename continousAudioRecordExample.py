import sounddevice as sd
import numpy as np
import scipy.signal
#import tensorflow as tf
import timeit

#Parameters
device_index = 2
num_channels = 2
debug_time = 1
window_length = 1.0
hop_length = 0.5
sample_rate = 48000
resample_rate = 16000

bufferLength = int(window_length * resample_rate)
audioBuffer = np.zeros(bufferLength)
hopRatio = window_length / hop_length

def downsample(signal, old_fs, new_fs):
    
    # Check to make sure we're downsampling
    if new_fs > old_fs:
        print("Error: target sample rate higher than original")
        return signal, old_fs
    
    # We can only downsample by an integer factor
    dec_factor = old_fs / new_fs
    if not dec_factor.is_integer():
        print("Error: can only decimate by integer factor")
        return signal, old_fs

    # Do decimation
    resampled_signal = scipy.signal.decimate(signal, int(dec_factor))

    return resampled_signal, new_fs

def sd_callback(rec, frames, time, status):

    # Start timing for testing
    start = timeit.default_timer()
    
    # Notify if errors
    if status:
        print('Error:', status)
    
    #print("Size of rec before squeeze is: ", len(rec))
    #print("Shape of rec before squeeze is: ", rec.shape)
    # Remove 2nd dimension from recording sample
    rec = np.squeeze(rec)
    #print("Size of rec after squeeze is: ", len(rec))
    #print("Shape of rec after squeeze is: ", rec.shape)

    # Resample
    rec, new_fs = downsample(rec, sample_rate, resample_rate)
    

    #print("Audio buffer locations: ", int(bufferLength//hopRatio))
    #print("Size of rec is: ", len(rec))
    # Save recording onto sliding window
    audioBuffer[:int(bufferLength//hopRatio)] = audioBuffer[int(bufferLength//hopRatio):]
    audioBuffer[int(bufferLength//hopRatio):] = rec

    if debug_time:
        print("Total time needed for callback in ms: ", (timeit.default_timer() - start)*1000)

    print("Average audio value of buffer is: ", np.average(audioBuffer))
    print()

with sd.InputStream(device=device_index, channels=num_channels,
                    samplerate=sample_rate,
                    blocksize=int(sample_rate * hop_length),
                    callback=sd_callback):
    while True:
        pass
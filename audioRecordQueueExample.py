import os, sys
import numpy as np
import sounddevice as sd
import time
import timeit
import queue
import threading

callbackTimeTracker = []

def listDevices():
    print("list devices")
    print(sd.query_devices(kind='input'))

def displayTimes(arrayCopyTime, hopLength, timeStart):
    global callbackTimeTracker
    print()
    print()
    print("Total time needed for inputstream callbacks: ", np.sum(callbackTimeTracker) * 1000)
    print("Amount of callbacks: ", len(callbackTimeTracker))
    callbackTimeTracker = []

    print("now longer than hop length!, Hop Length is: ", hopLength)
    print("Elapsed time is: ", (time.time() - timeStart))
    print("Total time needed for copying audio frames in ms: ", np.sum(arrayCopyTime) * 1000)
    print("Amount of window replacements: ", len(arrayCopyTime))

def classificationQueue(*, q, dataWindow, HL):
    print("start classificaiton queue")
    emptyCounter = 0
    arrayCopyTime = []

    timeStart = time.time()
    tempTracker = 0
    t = threading.current_thread()
    #print("current thread attributes: ", t.__dict__)
    #print("current thread keys: ", t.__dict__.keys())
    #print("Gettattribute T: ", getattr(t))
    #print("Get attribue t do run: ", getattr(t, "do_run"))


    while getattr(t, "do_run", True):
        try:
            #print("get attribute succes now replacing window....")
            #print("queue length: ", q.qsize())
            data = q.get_nowait()                                       # gets raw data from microphone (1136 samples long if no downsampling is applied)
            shift = len(data)
            print("length of data is: ", shift)
            windowsStartTime = timeit.default_timer()
            dataWindow = np.roll(dataWindow, -shift)            # dataWindow is total window length over which the features are calculated
            dataWindow[-shift:] = data                                  # new incoming raw microphone data is added to window
            #print("Time needed to replace window in ms: ", (timeit.default_timer() - windowsStartTime)*1000)
            #print("length of datawindow is: ", len(dataWindow))
            #print("shape of datawindow is: ", dataWindow.shape)
            #print("Window replaced")
            arrayCopyTime.append(timeit.default_timer() - windowsStartTime)

            print("is hoplength time voorbij?")
            if (time.time() - timeStart) >= HL:                          # every HL seconds a classification is made
                print("Audio features berekenen")
                #displayTimes(arrayCopyTime, HL, timeStart)
                arrayCopyTime = []

                timeStart = time.time()                                 # restart timer
                featuresAudio = np.max(dataWindow)    # calculate features
                print("Audio features/max: ", featuresAudio)

        except queue.Empty:
            emptyCounter = emptyCounter + 1
            #print("empty counter is: ", emptyCounter)

class classAudio:
    def __init__(self, FL, HL):
        print("class audio init")
        self.device = 14 #Laptop microphone
        #self.device = 5 #RPI
        self.downsample = 1
        self.samplerate = 44100
        self.mapping = 0                                # First channel has index 0

        print("queue init")
        self.q = queue.Queue()                          # Incoming audio is stacked in queue
        print("input stream init")                      #blocksize=int(HL * self.samplerate),
        self.stream = sd.InputStream(device=self.device, samplerate=self.samplerate, channels=1, blocksize=int(HL * self.samplerate), callback=self.callback)    # sound Stream thread, runs to constantly obtain microphone data

        length = int(float(FL) * self.samplerate / self.downsample)
        self.dataWindow = np.zeros(length)

        print("window length size: ", length)

        print("thread init")
        self.thread = threading.Thread(
            target=classificationQueue,
            kwargs=dict(
                q=self.q,
                dataWindow=self.dataWindow,
                HL=float(HL)
            ),
        )  # , args =(lambda : stop_threads, ))   # classification thread, constantly classifies incoming data

    def startMicControl(self):
        print("Start microphone control")
        self.stream.start()
        time.sleep(0.1)
        print("Start microphone thread control")
        self.thread.start()

    def stopMicControl(self):
        print("Stop microphone control")
        self.stream.stop()

        self.thread.do_run = False
        # self.thread.join()
        # self.stream.join()

    def callback(self, indata, frames, time, status):
        global callbackTimeTracker
        if status:
            print(status)
        #print("indata completetly is: ", indata)
        #print("indata length is: ", len(indata))
        #print("indata length is with downsample etc: ", len(indata[::self.downsample, self.mapping]))
        #print("indata shape is: ", indata.shape)
        #print("indata with downsample and mapping is: ", indata[::self.downsample, self.mapping])
        #print("indata downsample and mappingshape is: ", indata[::self.downsample, self.mapping].shape)
        #print("indata with squeeze also is: ", np.squeeze(indata[::self.downsample, self.mapping]))
        #print("indata with squeeze shape is: ", np.squeeze(indata[::self.downsample, self.mapping]).shape)
        callbackStartTime = timeit.default_timer()
        self.q.put(indata[::self.downsample, self.mapping])
        #print("Callback elapsed time is: ", (timeit.default_timer() - callbackStartTime)*1000)
        callbackTimeTracker.append(timeit.default_timer() - callbackStartTime)
        #print("queue length in callback is: ", self.q.qsize())

class Test:
    def __init__(self, parent=None):
        print("Init test 2")
        listDevices
        print(self)
        
        self.FL = None
        self.HL = None
        self.toStart = False

        self.startstopMic()

    def startstopMic(self):
        if self.toStart == True:
            print("start test")
            self.FL = 1
            self.HL = 0.5
            print("Initialize class audio")
            self.Insta = classAudio(self.FL, self.HL)
            print("start Mic control")
            self.Insta.startMicControl()
        else:
            print("stop test")
            self.Insta.stopMicControl()

    def setStatus(self, newStatus):
        self.toStart = newStatus
        print("status set to: ", newStatus)

def startTest():
    print("start test")
    FL = 1
    HL = 0.5
    print("Initialize class audio")
    Insta = classAudio(FL, HL)
    print("start Mic control")
    Insta.startMicControl()


print("ik ben de main, aan het beginnen:")
listDevices()
print("Init test")
testObject = Test
#startTest()
testObject.setStatus(testObject, True)
testObject.startstopMic(testObject)
mainStartTime = time.time()

while True:
    if (time.time() - mainStartTime) > 15:
        print("Stopping")
        testObject.setStatus(testObject, False)
        testObject.startstopMic(testObject)
        break
    #else:
        #print("time is now: ", (time.time() - mainStartTime))
sys.exit()
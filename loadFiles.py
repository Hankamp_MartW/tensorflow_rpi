#import tensorflow as tf
#import numpy as np

unknown_audio_directories = ['backward', 'bed', 'bird', 'cat', 'dog', 'eight', 'five', 'follow', 'forward', 'four', 'happy', 'house', 'learn', 'marvin', 'nine', 'one', 'seven',
                            'sheila', 'six', 'three', 'tree', 'two', 'visual', 'wow', 'zero', '_unknown_']
                            
train_with_unknown = True #Whether or not to include the unknown class to training
train_with_all_unknown = True #Results in better accuracy for certain sounds, less misclassifications. But makes training inbalanced, 10x as many unknown samples then other commands
add_noise = False #Validation set also gets noise added, test set doesn't get noise added

#dataset_directory_path = "/home/pi/speech_command_dataset/"  #Raspberry pi
#dataset_directory_path = "E:\Hankamp\small_speech_dataset\\" #Personal Laptop
dataset_directory_path = "C:\Hankamp\Raspberry Pi\small_speech_dataset\\" #Personal Dekstop

def load_filenames():
  filename_list = []
  #wav_ammounts = []

  for items in commands:
    filename_list = filename_list + (tf.io.gfile.glob(dataset_directory_path + items + '/*'))
    #wav_ammounts = len(tf.io.gfile.glob(dataset_directory_path + items + '/*'))

  filename_list = tf.random.shuffle(filename_list)
  return filename_list


if __name__ == '__main__':
  #commands = np.array(tf.io.gfile.listdir(dataset_directory_path))
  commands = ['up', 'stop', 'no', 'right', 'left', 'down', 'go', 'off', 'on', 'yes', '_unknown_'] #Has to be done this way in order to keep the order of commands
  print('Commands:', commands)

  filenames = load_filenames()
  num_samples = len(filenames)
  print('Number of total examples:', num_samples)
  print('Example file tensor:', filenames[0])
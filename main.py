#1: Load files(loadFiles.py)
#2: Load dataset (loadDataset.py)
#3: load model (loadExistingModel.py)

import os
import shutil
import pathlib

import matplotlib.pyplot as plt #conda install -c conda-forge matplotlib
import numpy as np #conda install numpy
import seaborn as sns #conda install seaborn

import tensorflow as tf #For laptop or dekstop, can use gpu version (conda install -c anaconda tensorflow-gpu)
from tensorflow.keras import layers #conda install -c conda-forge keras-gpu
from tensorflow.keras import models

from IPython import display #conda install ipython

from IPython.display import Audio
from pydub import AudioSegment, effects ##conda install -c conda-forge ipython, Also needs ffmpeg, not included if you download via conda install(conda install ffmpeg)
import time

seed = 42
tf.random.set_seed(seed)
np.random.seed(seed)

print("All modules and dependencies loaded")
#print(tf.config.list_physical_devices()) #Show all available devices for tensorflow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #Ignores message that a certain instruction set is not used.

#Test for workings of python and running other scripts
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
print()
print("-----------------------------------------------------------------------------------------------")
test_int = 20
exec(open("test1.py").read())   #See if test_int is available in the test1.py (it is)

print(other_test_int) #See if other_test_int, that is initialized in test1.py, is also available in test1.py (it is)
print("-----------------------------------------------------------------------------------------------")
print()
#End of test
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Test of load files.py
print("-----------------------------------------------------------------------------------------------")
exec(open("loadFiles.py").read())
print(filenames[0].numpy().decode('utf_8'))
print("-----------------------------------------------------------------------------------------------")
print()
#End of test
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Test of load dataset
print("-----------------------------------------------------------------------------------------------")
exec(open("loadDataset.py").read())
print("Test dataset: ", test_ds)
print("-----------------------------------------------------------------------------------------------")
print()
#End of test
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Spectogram show test
#def plot_spectrogram(spectrogram, ax):
#  if len(spectrogram.shape) > 2:
#    assert len(spectrogram.shape) == 3
#    spectrogram = np.squeeze(spectrogram, axis=-1) #tfl.squeeze
#  # Convert the frequencies to log scale and transpose, so that the time is
#  # represented on the x-axis (columns).
#  # Add an epsilon to avoid taking a log of zero.
#  log_spec = np.log(spectrogram.T + np.finfo(float).eps) #tfl.transpose, tfl.log, 
#  height = log_spec.shape[0] #tfl.shape
#  width = log_spec.shape[1]
#  X = np.linspace(0, np.size(spectrogram), num=width, dtype=int) #tfl.hashtable_size, 
#  Y = range(height)
#  ax.pcolormesh(X, Y, log_spec)
#
#rows = 3
#cols = 3
#n = rows*cols
#fig, axes = plt.subplots(rows, cols, figsize=(10, 10))
#
#for i, (spectrogram, label_id) in enumerate(test_ds.take(n)):
#  r = i // cols
#  c = i % cols
#  ax = axes[r][c]
#  plot_spectrogram(spectrogram.numpy(), ax)
#  ax.set_title(commands[label_id.numpy()])
#  ax.axis('off')
#
#plt.show()
#End of test
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Interference of model

#exec(open("loadExistingModel.py").read())
#print(commands)
#sample_file = str(dataset_directory_path) + '/dog/591d32f3_nohash_0.wav'
#print(sample_file)
#label_sample = tf.strings.split(input=sample_file, sep=os.path.sep)[-2]
#print(label_sample)

exec(open("loadExistingModel.py").read())

time_interference_array = []
predictions_array = []
true_labels = []
runcounter = 0

for spectrogram, label in test_ds.batch(1):
  start = time.perf_counter()
  #with tf.device('/device:CPU:0'):
    #prediction = model(spectrogram)
  prediction = model(spectrogram)
  time_interference_array.append(time.perf_counter() - start)
  ypred = np.argmax(prediction, axis=1)
  print("ARGMAX: ", ypred)
  #print("Only prediction no argmax: ", prediction)
  #print("Size of prediction tensor: ", tf.size(prediction))
  #print("Softmax: ", tf.nn.softmax(prediction[0]))
  predictions_array.append(ypred)
  true_labels.append(label.numpy())
  print("True label: ", label.numpy())

  runcounter += 1

print("Done...")
print("predictions: ", predictions_array[1])
print("Size of prediction: ", len(predictions_array))
print("True labels: ", true_labels[1])
print("Size of labels: ", len(true_labels))

predictions_array= np.array(predictions_array)
true_labels = np.array(true_labels)

accuracy = (sum(predictions_array == true_labels) / len(true_labels))[0]
print(f'Test set accuracy: {accuracy:.0%}')

print('Average interference time in milliseconds.', tf.reduce_mean(time_interference_array).numpy()*1000)
print("Runcounter is: ", runcounter)

predictions_array= np.squeeze(predictions_array)
true_labels = np.squeeze(true_labels)
print("Shape of pred array: ", predictions_array.shape)
print("Shape of true labels", true_labels.shape)
print("pred: ", predictions_array)
print("true: ", true_labels)
#End of test
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Confusion matrix
#confusion_mtx = tf.math.confusion_matrix(predictions_array, true_labels)
#plt.figure(figsize=(10, 8))
#sns.heatmap(confusion_mtx,
#            xticklabels=commands,
#            yticklabels=commands,
#            annot=True, fmt='g')
#plt.xlabel('Prediction')
#plt.ylabel('Label')
#plt.show()